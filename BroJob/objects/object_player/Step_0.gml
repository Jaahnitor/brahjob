/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
/// @DnDVersion : 1
/// @DnDHash : 25C45B21
/// @DnDArgument : "key" "vk_right"
var l25C45B21_0;
l25C45B21_0 = keyboard_check(vk_right);
if (l25C45B21_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 1A47EA12
	/// @DnDParent : 25C45B21
	/// @DnDArgument : "expr" "5"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "x"
	x += 5;
}

/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
/// @DnDVersion : 1
/// @DnDHash : 40E2E5BE
/// @DnDArgument : "key" "vk_left"
var l40E2E5BE_0;
l40E2E5BE_0 = keyboard_check(vk_left);
if (l40E2E5BE_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 3F5C379A
	/// @DnDParent : 40E2E5BE
	/// @DnDArgument : "expr" "-5"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "x"
	x += -5;
}

/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
/// @DnDVersion : 1
/// @DnDHash : 4FA3A80D
/// @DnDArgument : "key" "vk_up"
var l4FA3A80D_0;
l4FA3A80D_0 = keyboard_check(vk_up);
if (l4FA3A80D_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 59599495
	/// @DnDParent : 4FA3A80D
	/// @DnDArgument : "expr" "-5"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "y"
	y += -5;
}

/// @DnDAction : YoYo Games.Mouse & Keyboard.If_Key_Down
/// @DnDVersion : 1
/// @DnDHash : 55279103
/// @DnDArgument : "key" "vk_down"
var l55279103_0;
l55279103_0 = keyboard_check(vk_down);
if (l55279103_0)
{
	/// @DnDAction : YoYo Games.Common.Variable
	/// @DnDVersion : 1
	/// @DnDHash : 0037A96D
	/// @DnDParent : 55279103
	/// @DnDArgument : "expr" "5"
	/// @DnDArgument : "expr_relative" "1"
	/// @DnDArgument : "var" "y"
	y += 5;
}